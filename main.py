#!/usr/bin/env python3

"""Slide 32"""


class Product:
    pass


class Cosmetic(Product):
    pass


class Cream(Cosmetic):
    pass


"""Slide 41"""


def print_my_name():
    print("Andrzej")


print_my_name()


def print_sum():
    print(1+2+3)


print_sum()


def print_hello():
    print("Hello World")


print_hello()


def div(num1: int, num2: int) -> float:
    return num1/num2


print(div(25, 2))


class Test:
    pass


test_object = Test()


def return_test(value: Test) -> Test:
    return value


print(isinstance(return_test(test_object), Test))

"""Slide 50"""


class Human:
    def __init__(self, first_name: str, last_name: str, age: int):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def print_personal_data(self):
        print(self.first_name, " ", self.last_name, " born ", self.age, " years ago.")

    def change_age(self, new_age: int):
        self.age = new_age
        print("Age value change was succesful")


person1 = Human("Andrzej", "Osowski", "22")
person1.change_age(24)
person1.print_personal_data()


class Programmer(Human):
    def __init__(self, language: str, first_name: str, last_name: str, age: int):
        Human.__init__(self, first_name, last_name, age)
        self.language = language

    def change_age(self, new_age: int):
        self.age = new_age
        print("Age value change was succesful with overrided method")


person2=Programmer("Python", "John", "Kowalski", 59)
person2.print_personal_data()
person2.change_age(45)
person2.print_personal_data()

"""Slide 56"""


def get_information():
    first_name: str = input("What is your first name: ")
    second_name: str = input("What is your second name: ")
    age: int = input("How old are you?")
    print(first_name + " " + second_name + " " + age)


def get_float():
    float_number: str = input("Give me some float: ")
    float_number: float = float(float_number)
    print("float_number is instance: {}".format(type(float_number).__name__))


class Tulip:
    def __init__(self, color: str, age: int):
        self.color: str = color
        self.age: int = age

    def set_value(self):
        self.color: str = input("Please give me color")
        self.age: int = input("Please give me age")


first_tulip = Tulip("yellow", 1)
first_tulip.set_value()

"""Slide 71"""


def check_name():
    user_name: str = input("Put your name here: ")
    if user_name == "Andrzej":
        print("Hello")
    else:
        print("Hi")


class Rectangle:
    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.surfaceArea = a*b

    def __eq__(self, other):
        return self.surfaceArea == other.surfaceArea

    def __lt__(self, other):
        return self.surfaceArea < other.surfaceArea

    def __gt__(self, other):
        return self.surfaceArea > other.surfaceArea

    def __le__(self, other):
        return self.surfaceArea <= other.surfaceArea

    def __ge__(self, other):
        return self.surfaceArea >= other.surfaceArea

    def __ne__(self, other):
        return self.surfaceArea != other.surfaceArea


class Roller:
    def __init__(self, r, h):
        self.r = r
        self.h = h
        self.capacity = 3.14*r*r*h

    def __eq__(self, other):
        return self.capacity == other.capacity

    def __lt__(self, other):
        return self.capacity < other.capacity

    def __gt__(self, other):
        return self.capacity > other.capacity

    def __le__(self, other):
        return self.capacity <= other.capacity

    def __ge__(self, other):
        return self.capacity >= other.capacity

    def __ne__(self, other):
        return self.capacity != other.capacity



class WheeledVehicles:
    def __init__(self, brand, model, color):
        self.brand = brand
        self.model = model
        self.color = color

    def __eq__(self, other):
        return (self.brand == other.brand) and (self.model == other.model) and (self.color == other.color)

    def __ne__(self, other):
        return (self.brand != other.brand) and (self.model != other.model) and (self.color != other.color)


class Motorcycle(WheeledVehicles):
    def __init__(self, brand, model, color, engine_capacity):
        super().__init__(brand, model, color)
        self.engine_capacity = engine_capacity

    def __eq__(self, other):
        return super().__eq__(other) and (self.engine_capacity == other.engine_capacity)

    def __ne__(self, other):
        return super().__eq__(other) and (self.engine_capacity != other.engine_capacity)

    def __gt__(self, other):
        return self.engine_capacity > other.engine_capacity

    def __lt__(self, other):
        return self.engine_capacity < other.engine_capacity

    def __le__(self, other):
        return self.engine_capacity <= other.engine_capacity

    def __ge__(self, other):
        return self.engine_capacity >= other.engine_capacity


class Car(WheeledVehicles):
    def __init__(self, brand, model, color, num_of_doors):
        super().__init__(brand, model, color)
        self.num_of_doors = num_of_doors

    def __eq__(self, other):
        return super().__eq__(other) and (self.num_of_doors == other.num_of_doors)

    def __ne__(self, other):
        return super().__eq__(other) and (self.num_of_doors != other.num_of_doors)

    def __gt__(self, other):
        return self.num_of_doors > other.num_of_doors

    def __lt__(self, other):
        return self.num_of_doors < other.num_of_doors

    def __le__(self, other):
        return self.num_of_doors <= other.num_of_doors

    def __ge__(self, other):
        return self.num_of_doors >= other.num_of_doors


class Truck(WheeledVehicles):
    def __init__(self, brand, model, color, permissible_gross_weight):
        super().__init__(brand, model, color)
        self.permissible_gross_weight = permissible_gross_weight

    def __eq__(self, other):
        return super().__eq__(other) and (self.permissible_gross_weight == other.permissible_gross_weight)

    def __ne__(self, other):
        return super().__eq__(other) and (self.permissible_gross_weight != other.permissible_gross_weight)

    def __gt__(self, other):
        return self.permissible_gross_weight > other.permissible_gross_weight

    def __lt__(self, other):
        return self.permissible_gross_weight < other.permissible_gross_weight

    def __le__(self, other):
        return self.permissible_gross_weight <= other.permissible_gross_weight

    def __ge__(self, other):
        return self.permissible_gross_weight >= other.permissible_gross_weight


class SportCar(Car):
    def __init__(self, brand, model, color, num_of_doors, max_speed):
        super().__init__(brand, model, color, num_of_doors)
        self.max_speed = max_speed

    def __eq__(self, other):
        return super().__eq__(other) and (self.max_speed == other.max_speed)

    def __ne__(self, other):
        return super().__eq__(other) and (self.max_speed != other.max_speed)

    def __gt__(self, other):
        return super().__gt__(other) and self.max_speed > other.max_speed

    def __lt__(self, other):
        return super().__lt__(other) and self.max_speed < other.max_speed

    def __le__(self, other):
        return super().__le__(other) and self.max_speed <= other.max_speed

    def __ge__(self, other):
        return super().__ge__(other) and self.max_speed >= other.max_speed


class CityCar(Car):
    def __init__(self, brand, model, color, num_of_doors, fuel_consumption):
        super().__init__(brand, model, color, num_of_doors)
        self.fuel_consumption = fuel_consumption

    def __eq__(self, other):
        return super().__eq__(other) and (self.fuel_consumption == other.fuel_consumption)

    def __ne__(self, other):
        return super().__eq__(other) and (self.fuel_consumption != other.fuel_consumption)

    def __gt__(self, other):
        return super().__gt__(other) and self.fuel_consumption > other.fuel_consumption

    def __lt__(self, other):
        return super().__lt__(other) and self.fuel_consumption < other.fuel_consumption

    def __le__(self, other):
        return super().__le__(other) and self.fuel_consumption <= other.fuel_consumption

    def __ge__(self, other):
        return super().__ge__(other) and self.fuel_consumption >= other.fuel_consumption


"""Slide 82"""
first_list = ([1], [2], [3])
empty_list = list()
for num in range(100, 115, 1):
    empty_list.append(num)

float_list = (3.14, 2.14, 4.14, 5.14, 1.14)
for num in range(0, 5, 1):
    print(float_list[num])
